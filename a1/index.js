const http = require('http');

// Create a variable "port" to store the port number
const port = 3000;

// Creates a variable "server" that stores the output of the "createServer" method
const server = http.createServer((request, response) => {

	if (request.url == '/login') {
		response.writeHead(200, {'Content-Type': 'text/plain'})
		response.end('Welcome to the login page')
	}  else {
		response.writeHead(404, {'Content-Type': 'text/plain'})
		response.end("I'm sorry the page you are looking for cannot be found")
	}

})

// Uses the "server" and "port" variables created above
server.listen(port)

// When server is running, console will print the message.
console.log(`Server is running`)
